# ***Best Album of All Times***
---
![J.Cole 2014 Forest Hills Drive Album Cover](2014_Forest_Hills_Drive.jpg)
## *Best Songs in Album By Ranking*
---
1. **No Role Modelz**
2. **Wet Dreamz**
5. **A Tale of 2 Citiez**
3. **G.O.M.D.**
4. **Fire Squad**
### ***More in Depth Analysis***
---
- *No Role Modelz*
> "No role models and I'm here right now / No role models to speak of / Searching through my memory,
> my memory, and I couldn't find one"

One of the truest statements throughout the entire song. Role models have become harder and harder
to find. Drake said it best in his Lemon Pepper Freestyle,

> "These days, fame is disconnected from excellence"
---
- *Wet Dreamz*
> "Got me daydreamin', man, what / I'm thinkin' how she rides on it, if she sits on it, if she licks on it / 
Make it hard for me to stand up, as time goes by / Attractions gettin' deep and / Wet dreamin', thinkin'
that  / I'm smashin' but I'm sleepin' / I want it bad, and I ain't never been obsessed before / 
She wrote a note that said, "You ever had sex before?" / Damn…"

Lets be real, every guy has been their this song was an open book, as much of this album was designed to be.
Cole references one of his first crushed, nevermind the fact this was about losing his virginity as a child.
Cole was just up front about the process he and most guys go through before the first time, and it ends with 
Cole realizing his crush was just as nervous about it.

---
- *G.O.M.D*

Do I even need to mention anything, we all love this song!?

---
- *Fire Squad*
> Ain't a way around it no more, I am the greatest / A lotta n****as sat on the throne, I am the latest
 / I am the bravest, go toe to toe with the giants

Cole literally claimed a title that he clearly deserved and has proven in all of his music that has followed. 
You have to respect a man that already knows where he is headed.

---
- *A Tale of 2 Citiez*
> Small town n***a Hollywood dreams / I know that everything that glitters ain't gold
 / I know the shit ain't always good as it seems / But tell me till you get it how could you know?
 / How could you know? How could you know?

Cole talks warns of the problems that comes with fame and money, and is saying that the less fortunate often 
look at the wealthy like they don't have the same problems as the poor.

---
## Notable Awards
This album won 2 different awards, the *Billboard Music Award for Top Rap Album* and *BET Best Hip Hop Album
of the Year* in 2015. This was also the first Rap Album in history to win these awards without a single
feature on the album. If that doesn't G.O.A.T this album then you might have to give it a listen.
### Track List:

| **Song** | **Song Length** |
| :----: | :----: |
| Intro | 2:09 |
| January 28th | 4:02 |
| ***Wet Dreamz*** | 3:59 |
| 03' Adolesence| 4:24 |
| ***A Tale of 2 Citiez*** | 4:29 |
| ***Fire Sqaud*** | 4:48 |
| St. Topez | 4:17 |
| ***G.O.M.D*** | 5:01 |
| ***No Role Modelz*** | 4:52 |
| Hello | 3:39 |
| Apparently | 4:53 |
| Love Yourz | 3:31 |
| Note To Self | 14:35 |
